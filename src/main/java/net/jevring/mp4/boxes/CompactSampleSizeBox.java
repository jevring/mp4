package net.jevring.mp4.boxes;

import net.jevring.mp4.BoxReader;
import net.jevring.mp4.BoxWriter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author markus@jevring.net
 */
public class CompactSampleSizeBox extends FullBox {
	private final int fieldSize;
	private final int sampleCount;
	private final List<Integer> entrySizes = new ArrayList<>();

	public CompactSampleSizeBox(int size, Box container, BoxReader boxReader) throws IOException {
		super(size, "stz2", container, false, boxReader);
		boxReader.skip(3); // reserved
		fieldSize = boxReader.getSingleByteInteger();
		sampleCount = boxReader.getInteger();
		for (int i = 0; i < sampleCount; i++) {
			switch (fieldSize) {
				case 4:
					byte b = boxReader.getBytes(1)[0];
					entrySizes.add(b << 4);
					entrySizes.add(b & 0xf);
					break;
				case 8:
					entrySizes.add(boxReader.getSingleByteInteger());
					break;
				case 16:
					entrySizes.add((int) boxReader.getShort());
					break;
			}
		}
	}

	@Override
	public int calculateSize() {
		int entrySizes = 0;
		switch (fieldSize) {
			case 4:
				entrySizes = this.entrySizes.size() / 2;
			case 8:
				entrySizes = this.entrySizes.size();
				break;
			case 16:
				entrySizes = this.entrySizes.size() * 2;
				break;
		}

		return super.calculateSize() + 3 + 1 + 4 + entrySizes;
	}

	@Override
	public void write(BoxWriter out) throws IOException {
		super.write(out);
		out.skip(3);
		out.writeSingleByteInteger(sampleCount);
		out.writeInteger(sampleCount);
		for (int i = 0; i < sampleCount; i++) {
			switch (fieldSize) {
				case 4:
					byte b = 0;
					// todo: do the inverse of this
					//entrySizes.add(b << 4);
					//entrySizes.add(b & 0xf);
					// when that's done, remove this, so we don't write too much
					i++;
					out.skip(1);
					break;
				case 8:
					out.writeSingleByteInteger(entrySizes.get(i));
					break;
				case 16:
					out.writeShort(entrySizes.get(i).shortValue());
					break;
			}
		}
	}

	@Override
	public String toString() {
		return super.toString() + "{" +
				"fieldSize=" + fieldSize +
				", sampleCount=" + sampleCount +
				", entrySizes=" + entrySizes +
				'}';
	}
}
