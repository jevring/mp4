package net.jevring.mp4.boxes;

import net.jevring.mp4.BoxReader;
import net.jevring.mp4.BoxWriter;

import java.io.IOException;
import java.util.Arrays;

/**
 * @author markus@jevring.net
 */
public class FullBox extends Box {
	protected final int version;
	protected final byte[] flags;

	public FullBox(int size, String type, Box container, boolean canContainChildren, BoxReader boxReader) throws IOException {
		super(size, type, container, canContainChildren);
		version = boxReader.getSingleByteInteger();
		flags = boxReader.getBytes(3);
	}
	
	public FullBox(int size, String type, Box container, boolean canContainChildren, int version, byte[] flags) {
		super(size, type, container, canContainChildren);
		this.version = version;
		this.flags = flags;
	}

	public int getVersion() {
		return version;
	}

	public byte[] getFlags() {
		return flags;
	}

	@Override
	public int calculateSize() {
		return super.calculateSize() + 1 + 3;
	}

	@Override
	public void write(BoxWriter out) throws IOException {
		super.write(out);
		out.writeSingleByteInteger(version);
		out.writeBytes(flags);
	}

	/*
	public String toString() {
		return super.toString() + String.format("{version=%d, flags=%s}", version, Arrays.toString(flags));
	}
	*/
}
