package net.jevring.mp4.boxes;

import net.jevring.mp4.BoxReader;
import net.jevring.mp4.BoxWriter;

import java.io.IOException;
import java.util.Arrays;

/**
 * @author markus@jevring.net
 */
public class VideoMediaHeaderBox extends FullBox {
	private final short graphicsMode;
	private final short[] opcolor = new short[3];

	public VideoMediaHeaderBox(int size, Box container, BoxReader boxReader) throws IOException {
		super(size, "vmhd", container, false, boxReader);
		graphicsMode = boxReader.getShort();
		opcolor[0] = boxReader.getShort();
		opcolor[1] = boxReader.getShort();
		opcolor[2] = boxReader.getShort();
	}

	@Override
	public int calculateSize() {
		return super.calculateSize() + 2 + (3 * 2);
	}

	@Override
	public void write(BoxWriter out) throws IOException {
		super.write(out);
		out.writeShort(graphicsMode);
		for (short i : opcolor) {
			out.writeShort(i);
		}
	}

	@Override
	public String toString() {
		return super.toString() + "{" +
				"graphicsMode=" + graphicsMode +
				", opcolor=" + Arrays.toString(opcolor) +
				'}';
	}
}
