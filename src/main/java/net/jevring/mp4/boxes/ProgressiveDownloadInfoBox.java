package net.jevring.mp4.boxes;

import net.jevring.mp4.BoxReader;
import net.jevring.mp4.BoxWriter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author markus@jevring.net
 */
public class ProgressiveDownloadInfoBox extends FullBox {
	private final List<RateAndInitialDelay> rateAndInitialDelays = new ArrayList<>();

	public ProgressiveDownloadInfoBox(int size, Box container, BoxReader boxReader) throws IOException {
		super(size, "pdin", container, false, boxReader);
		int numberOfRateDelayEntries = (int) ((size - boxReader.getOffset()) / 2);
		for (int i = 0; i < numberOfRateDelayEntries; i++) {
			int rate = boxReader.getInteger();
			int initialDelay = boxReader.getInteger();
			rateAndInitialDelays.add(new RateAndInitialDelay(rate, initialDelay));
		}
	}

	@Override
	public int calculateSize() {
		return super.calculateSize() + (rateAndInitialDelays.size() * (4 + 4));
	}

	@Override
	public void write(BoxWriter out) throws IOException {
		super.write(out);
		for (RateAndInitialDelay rateAndInitialDelay : rateAndInitialDelays) {
			out.writeInteger(rateAndInitialDelay.rate);
			out.writeInteger(rateAndInitialDelay.delay);
		}
	}

	@Override
	public String toString() {
		return super.toString() + rateAndInitialDelays.toString();
	}

	private static final class RateAndInitialDelay {
		private final int rate;
		private final int delay;

		private RateAndInitialDelay(int rate, int delay) {
			this.rate = rate;
			this.delay = delay;
		}
	}

}
