package net.jevring.mp4.boxes;

import net.jevring.mp4.BoxReader;
import net.jevring.mp4.BoxWriter;

import java.io.IOException;

/**
 * @author markus@jevring.net
 */
public class SoundMediaHeaderBox extends FullBox {
	private final short balance;

	public SoundMediaHeaderBox(int size, Box container, BoxReader boxReader) throws IOException {
		super(size, "smhd", container, false, boxReader);
		balance = boxReader.getShort();
		boxReader.getShort(); // reserved
	}

	@Override
	public int calculateSize() {
		return super.calculateSize() + 2 + 2;
	}

	@Override
	public void write(BoxWriter out) throws IOException {
		super.write(out);
		out.writeShort(balance);
		out.skip(2);
	}

	@Override
	public String toString() {
		return super.toString() + "{" +
				"balance=" + balance +
				'}';
	}
}
