package net.jevring.mp4.boxes;

import net.jevring.mp4.BoxReader;
import net.jevring.mp4.BoxWriter;

import java.io.IOException;

/**
 * @author markus@jevring.net
 */
public class MediaHeaderBox extends FullBox {
	private final long creationTime;
	private final long modificationTime;
	private final int timescale;
	private final long duration;

	public MediaHeaderBox(int size, Box container, BoxReader boxReader) throws IOException {
		super(size, "mdhd", container, false, boxReader);
		if (version == 1) {
			creationTime = boxReader.getLong();
			modificationTime = boxReader.getLong();
			timescale = boxReader.getInteger();
			duration = boxReader.getLong();
		} else {
			creationTime = boxReader.getInteger();
			modificationTime = boxReader.getInteger();
			timescale = boxReader.getInteger();
			duration = boxReader.getInteger();
		}
		boxReader.skip(4); // todo: replace this with whatever horrific format is defined below.
		/*
				bit(1) pad = 0;
				unsigned int(5)[3] language; // ISO-639-2/T language code
				unsigned int(16) pre_defined = 0;


				 WTF? 5-bit ints?
				 */
	}

	@Override
	public int calculateSize() {
		int i = 0;
		if (version == 1) {
			i += 8 + 8 + 4 + 8;
		} else {
			i += 4 + 4 + 4 + 4;
		}
		i += 4;
		return super.calculateSize() + i;
	}

	@Override
	public void write(BoxWriter out) throws IOException {
		super.write(out);
		if (version == 1) {
			out.writeLong(creationTime);
			out.writeLong(modificationTime);
			out.writeInteger(timescale);
			out.writeLong(duration);
		} else {
			out.writeInteger((int) creationTime);
			out.writeInteger((int) modificationTime);
			out.writeInteger(timescale);
			out.writeInteger((int) duration);
		}
		out.skip(4);
	}

	@Override
	public String toString() {
		return super.toString() + "{" +
				"creationTime=" + creationTime +
				", modificationTime=" + modificationTime +
				", timescale=" + timescale +
				", duration=" + duration +
				'}';
	}
}
