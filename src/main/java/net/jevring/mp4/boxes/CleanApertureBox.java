package net.jevring.mp4.boxes;

import net.jevring.mp4.BoxReader;
import net.jevring.mp4.BoxWriter;

import java.io.IOException;

/**
 * @author markus@jevring.net
 */
public class CleanApertureBox extends Box {
	private final int cleanApertureWidthN;
	private final int cleanApertureWidthD;

	private final int cleanApertureHeightN;
	private final int cleanApertureHeightD;


	private final int horizOffN;
	private final int horizOffD;

	private final int vertOffN;
	private final int vertOffD;

	public CleanApertureBox(int size, Box container, BoxReader boxReader) throws IOException {
		super(size, "clap", container, false);
		cleanApertureWidthN = boxReader.getInteger();
		cleanApertureWidthD = boxReader.getInteger();
		cleanApertureHeightN = boxReader.getInteger();
		cleanApertureHeightD = boxReader.getInteger();
		horizOffN = boxReader.getInteger();
		horizOffD = boxReader.getInteger();
		vertOffN = boxReader.getInteger();
		vertOffD = boxReader.getInteger();
	}

	@Override
	public int calculateSize() {
		return super.calculateSize() + (8 * 4);
	}

	@Override
	public void write(BoxWriter out) throws IOException {
		super.write(out);
		out.writeIntegers(cleanApertureWidthN,
		                  cleanApertureWidthD,
		                  cleanApertureHeightN,
		                  cleanApertureHeightD,
		                  horizOffN,
		                  horizOffD,
		                  vertOffN,
		                  vertOffD);
	}

	@Override
	public String toString() {
		return super.toString() + "{" +
				"cleanApertureWidthN=" + cleanApertureWidthN +
				", cleanApertureWidthD=" + cleanApertureWidthD +
				", cleanApertureHeightN=" + cleanApertureHeightN +
				", cleanApertureHeightD=" + cleanApertureHeightD +
				", horizOffN=" + horizOffN +
				", horizOffD=" + horizOffD +
				", vertOffN=" + vertOffN +
				", vertOffD=" + vertOffD +
				'}';
	}
}
