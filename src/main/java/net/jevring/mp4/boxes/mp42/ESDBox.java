package net.jevring.mp4.boxes.mp42;

import net.jevring.mp4.BoxReader;
import net.jevring.mp4.boxes.Box;
import net.jevring.mp4.boxes.FullBox;

import java.io.IOException;

/**
 * @author markus@jevring.net
 */
public class ESDBox extends FullBox {
	// check gpac/include/gpac/mpeg4_odf.h:668
	public ESDBox(int size, Box container, BoxReader boxReader) throws IOException {
		super(size, "esds", container, false, boxReader);
	}
}
