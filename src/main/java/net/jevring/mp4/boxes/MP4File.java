package net.jevring.mp4.boxes;

import net.jevring.mp4.TrackBox;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author markus@jevring.net
 */
public class MP4File extends Box {
	private final Path path;
	
	public MP4File(String filename) {
		super(-1, "File", null, true);
		path = Paths.get(filename);
	}

	public Path getPath() {
		return path;
	}

	public Collection<Box> getBoxes() {
		List<Box> boxes = new ArrayList<>();
		enumerateBoxes(this, boxes);
		return boxes;
	}

	private void enumerateBoxes(Box box, List<Box> boxes) {
		for (Box child : box.children) {
			boxes.add(child);
			enumerateBoxes(child, boxes);
		}
	}

	/**
	 * Gets the i:th track
	 * @param i the number of the track to get
	 * @return i:th track box
	 */
	public TrackBox getTrack(int i) {
		Box b = this;
		b = b.getChildOfType("moov");
		for (Box child : b.children) {
			if (child.type.equals("trak")) {
				TrackBox track = (TrackBox) child;
				if (track.getTrackId() == i) {
					return track;
				}
			}
		}
		return null;
	}
	
	public List<TrackBox> getTracks() {
		List<TrackBox> tracks = new ArrayList<>();
		Box b = this;
		b = b.getChildOfType("moov");
		for (Box child : b.children) {
			if (child.type.equals("trak")) {
				TrackBox track = (TrackBox) child;
				tracks.add(track);
			}
		}
		return tracks;
	}
}
