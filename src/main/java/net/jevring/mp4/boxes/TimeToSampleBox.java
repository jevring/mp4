package net.jevring.mp4.boxes;

import net.jevring.mp4.BoxReader;
import net.jevring.mp4.BoxWriter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author markus@jevring.net
 */
public class TimeToSampleBox extends FullBox {
	private final int entryCount;
	private final List<Sample> samples = new ArrayList<>();

	public TimeToSampleBox(int size, Box container, BoxReader boxReader) throws IOException {
		super(size, "stts", container, false, boxReader);
		entryCount = boxReader.getInteger();

		for (int i = 0; i < entryCount; i++) {
			int sampleCount = boxReader.getInteger();
			int sampleDelta = boxReader.getInteger();
			samples.add(new Sample(sampleCount, sampleDelta));
		}
	}

	@Override
	public int calculateSize() {
		return super.calculateSize() + 4 + (entryCount * (4 + 4));
	}

	@Override
	public void write(BoxWriter out) throws IOException {
		super.write(out);
		out.writeInteger(entryCount);
		for (Sample sample : samples) {
			out.writeInteger(sample.sampleCount);
			out.writeInteger(sample.sampleDelta);
		}
	}

	@Override
	public String toString() {
		return super.toString() + "{" +
				"entryCount=" + entryCount +
				//", samples=<" + samples.size() + ">" +
				", samples=" + samples +
				'}';
	}

	private static class Sample {
		private final int sampleCount;
		private final int sampleDelta;

		private Sample(int sampleCount, int sampleDelta) {
			this.sampleCount = sampleCount;
			this.sampleDelta = sampleDelta;
		}

		@Override
		public String toString() {
			return "Sample{" +
					"count=" + sampleCount +
					", delta=" + sampleDelta +
					'}';
		}
	}
}

