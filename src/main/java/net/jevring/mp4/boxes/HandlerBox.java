package net.jevring.mp4.boxes;

import net.jevring.mp4.BoxReader;
import net.jevring.mp4.BoxWriter;

import java.io.IOException;

/**
 * @author markus@jevring.net
 */
public class HandlerBox extends FullBox {
	private final String handlerType;
	private final String name;

	public HandlerBox(int size, Box container, BoxReader boxReader) throws IOException {
		super(size, "hdlr", container, false, boxReader);
		boxReader.getInteger(); // pre_defined = 0, skipped
		handlerType = boxReader.get4ByteString();
		boxReader.skip(4 * 3); // reserved
		name = boxReader.getNullTerminatedString();
	}

	public String getHandlerType() {
		return handlerType;
	}

	@Override
	public int calculateSize() {
		return super.calculateSize() + 4 + 4 + (4 * 3) + name.length() + 1;
	}

	@Override
	public void write(BoxWriter out) throws IOException {
		super.write(out);
		out.skip(4);
		out.write4ByteString(handlerType);
		out.skip(4 * 3);
		out.writeNullTerminatedString(name);
	}

	@Override
	public String toString() {
		return super.toString() + "{" +
				"handlerType='" + handlerType +
				"', name='" + name + '\'' +
				'}';
	}
}
