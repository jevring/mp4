package net.jevring.mp4.boxes.sample.mp42;

import net.jevring.mp4.BoxReader;
import net.jevring.mp4.boxes.Box;
import net.jevring.mp4.boxes.mp42.ESDBox;
import net.jevring.mp4.boxes.sample.AudioSampleEntry;

import java.io.IOException;

/**
 * @author markus@jevring.net
 */
public class MP4AudioSampleEntry extends AudioSampleEntry {
	//private final ESDBox ed;

	public MP4AudioSampleEntry(int size, BoxReader boxReader) throws IOException {
		super(size, "mp4a", boxReader);
	}
}
