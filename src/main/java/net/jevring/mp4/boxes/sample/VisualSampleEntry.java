package net.jevring.mp4.boxes.sample;

import net.jevring.mp4.BoxReader;
import net.jevring.mp4.BoxWriter;
import net.jevring.mp4.boxes.Box;
import net.jevring.mp4.boxes.CleanApertureBox;
import net.jevring.mp4.boxes.PixelAspectRatioBox;

import java.io.IOException;

/**
 * @author markus@jevring.net
 */
public class VisualSampleEntry extends SampleEntry {
	private final short width;
	private final short height;
	private final int horizontalResolution;
	private final int verticalResolution;
	private final short frameCount;
	private final String compressorName;
	private final short depth;
	private CleanApertureBox clap;
	private PixelAspectRatioBox pasp;
	private final byte[] leftovers; // wtf is this?


	public VisualSampleEntry(int size, String codingName, BoxReader boxReader) throws IOException {
		super(size, codingName, boxReader);
		// -8 for box
		// -8 for sample entry
		long startingOffset = boxReader.getOffset() - 8 - 8; // this is where the entry started, minus the size and type already read
		long expectedEndOffset = startingOffset + size;
		boxReader.getShort(); // pre_defined = 0
		boxReader.getShort(); // reserved
		boxReader.skip(4 * 3); // pre_defined = 0
		width = boxReader.getShort();
		height = boxReader.getShort();
		horizontalResolution = boxReader.getInteger();
		verticalResolution = boxReader.getInteger();
		boxReader.getInteger(); // reserved
		frameCount = boxReader.getShort();
		compressorName = boxReader.getLengthPrefixedString(32);
		depth = boxReader.getShort();
		boxReader.getShort(); // pre_defined = -1
		/* todo: for some reason there's no clap or pasp here, there's just some other data

				System.out.println("boxReader.getOffset(), expectedEndOffset = " + boxReader.getOffset() + ", " + expectedEndOffset);
				while (boxReader.getOffset() < expectedEndOffset) {
					int entrySize = boxReader.getInteger();
					String entryType = boxReader.get4ByteString();
					System.out.println("entrySize = " + entrySize);
					System.out.println("entryType = " + entryType);
					if ("clap".equals(entryType)) {
						clap = new CleanApertureBox(entrySize, this, boxReader);
					} else if ("pasp".equals(entryType)) {
						pasp = new PixelAspectRatioBox(entrySize, this, boxReader);
					}
				}
				*/
		//System.out.println("boxReader.getString(expectedEndOffset - boxReader.getOffset()) = " + boxReader.getString((int) (expectedEndOffset - boxReader.getOffset())));
		// NOTE: Apparently, any data after the required fields is to be treated as initialization data to
		// whatever codec is being specified in the whatever field.
		// unknown data at the end of a box shall be ignored
		final long remainingBytes = expectedEndOffset - boxReader.getOffset();
		System.out.println("Remaining bytes: " + remainingBytes);
		leftovers = boxReader.getBytes((int) remainingBytes);
		System.out.println("leftovers = " + new String(leftovers));
	}

	@Override
	public void write(BoxWriter out) throws IOException {
		super.write(out);
		out.skip(2 + 2 + (4 * 3));
		out.writeShort(width);
		out.writeShort(height);
		out.writeInteger(horizontalResolution);
		out.writeInteger(verticalResolution);
		out.skip(4);
		out.writeShort(frameCount);
		out.writePaddedString(compressorName, 32);
		out.writeShort(depth);
		out.skip(2);
		out.writeBytes(leftovers);
	}

	@Override
	public int calculateSize() {
		int s = 0;
		s += 2 + 2 + (4 * 3); // skip
		s += 2 + 2 + 4 + 4 + 4 + 2 + 32 + 2 + 2;
		return super.calculateSize() + s + leftovers.length;
	}

	@Override
	public String toString() {
		return super.toString() + "{" +
				"width=" + width +
				", height=" + height +
				", horizontalResolution=" + horizontalResolution +
				", verticalResolution=" + verticalResolution +
				", frameCount=" + frameCount +
				", compressorName='" + compressorName + '\'' +
				", depth=" + depth +
				", clap=" + clap +
				", pasp=" + pasp +
				'}';
	}
}
