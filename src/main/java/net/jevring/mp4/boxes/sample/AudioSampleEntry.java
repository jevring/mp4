package net.jevring.mp4.boxes.sample;

import net.jevring.mp4.BoxReader;
import net.jevring.mp4.BoxWriter;
import net.jevring.mp4.boxes.Box;

import java.io.IOException;

/**
 * @author markus@jevring.net
 */
public class AudioSampleEntry extends SampleEntry {
	private final short channelCount;
	private final short sampleSize;
	private final int sampleRate;
	private final byte[] leftovers; // wtf is this?


	public AudioSampleEntry(int size, String codingName, BoxReader boxReader) throws IOException {
		super(size, codingName, boxReader);
		// -8 for box
		// -8 for sample entry
		long startingOffset = boxReader.getOffset() - 8 - 8; // this is where the entry started, minus the size and type already read
		long expectedEndOffset = startingOffset + size;
		boxReader.skip(4 * 2); // reserved
		channelCount = boxReader.getShort();
		sampleSize = boxReader.getShort();
		boxReader.getShort(); // pre_defined = 0
		boxReader.getShort(); // reserved
		sampleRate = boxReader.getInteger(); // todo: this is way off. or rather, it's negative. does this really need to be unsigned? is it that large?
		final long remainingBytes = expectedEndOffset - boxReader.getOffset();
		System.out.println("Remaining bytes: " + remainingBytes);
		leftovers = boxReader.getBytes((int) remainingBytes);
		System.out.println("leftovers = " + new String(leftovers));

		// todo: libmp4.h:437.
		// seems there's some more info in here
	}

	@Override
	public int calculateSize() {
		return super.calculateSize() + (4 * 2) + 2 + 2 + 2 + 2 + 4 + leftovers.length;
	}

	@Override
	public void write(BoxWriter out) throws IOException {
		super.write(out);
		out.skip(4 * 2);
		out.writeShort(channelCount);
		out.writeShort(sampleSize);
		out.skip(2 + 2);
		out.writeInteger(sampleRate);
		out.writeBytes(leftovers);
	}

	@Override
	public String toString() {
		return super.toString() + "{" +
				"channelCount=" + channelCount +
				", sampleSize=" + sampleSize +
				", sampleRate=" + sampleRate +
				'}';
	}
}
