package net.jevring.mp4.boxes.sample;

import net.jevring.mp4.BoxReader;
import net.jevring.mp4.BoxWriter;
import net.jevring.mp4.boxes.Box;

import java.io.IOException;

/**
 * @author markus@jevring.net
 */
public abstract class SampleEntry extends Box {
	private final short dataReferenceIndex;

	public SampleEntry(int size, String format, BoxReader boxReader) throws IOException {
		super(size, format, null, false);
		boxReader.skip(6); // reserved
		dataReferenceIndex = boxReader.getShort();
	}

	@Override
	public int calculateSize() {
		return super.calculateSize() + 6 + 2;
	}

	@Override
	public void write(BoxWriter out) throws IOException {
		super.write(out);
		out.skip(6);
		out.writeShort(dataReferenceIndex);
	}

	@Override
	public String toString() {
		return super.toString() + "{" +
				"dataReferenceIndex=" + dataReferenceIndex +
				'}';
	}
}
