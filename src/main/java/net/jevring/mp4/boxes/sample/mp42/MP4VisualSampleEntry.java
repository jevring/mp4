package net.jevring.mp4.boxes.sample.mp42;

import net.jevring.mp4.BoxReader;
import net.jevring.mp4.boxes.Box;
import net.jevring.mp4.boxes.mp42.ESDBox;
import net.jevring.mp4.boxes.sample.VisualSampleEntry;

import java.io.IOException;

/**
 * @author markus@jevring.net
 */
public class MP4VisualSampleEntry extends VisualSampleEntry {
	//private final ESDBox ed;
	public MP4VisualSampleEntry(int size, BoxReader boxReader) throws IOException {
		super(size, "mp4v", boxReader);
	}
}
