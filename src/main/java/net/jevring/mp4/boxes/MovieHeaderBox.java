package net.jevring.mp4.boxes;

import net.jevring.mp4.BoxReader;
import net.jevring.mp4.BoxWriter;

import java.io.IOException;

/**
 * @author markus@jevring.net
 */
public class MovieHeaderBox extends FullBox {
	private final long creationTime;
	private final long modificationTime;
	private final int timescale;
	private final long duration;
	private final int rate;
	private final short volume;
	private final int[] matrix = new int[]{0x00010000, 0, 0, 0, 0x00010000, 0, 0, 0, 0x40000000};
	private final int[] preDefined = new int[]{0, 0, 0, 0, 0, 0}; // don't know what this is...
	private int nextTrackId;

	public MovieHeaderBox(int size, Box container, BoxReader boxReader) throws IOException {
		super(size, "mvhd", container, false, boxReader);
		if (version == 1) {
			creationTime = boxReader.getLong();
			modificationTime = boxReader.getLong();
			timescale = boxReader.getInteger();
			duration = boxReader.getLong();
		} else {
			creationTime = boxReader.getInteger();
			modificationTime = boxReader.getInteger();
			timescale = boxReader.getInteger();
			duration = boxReader.getInteger();
		}
		rate = boxReader.getInteger(); // template: 0x00010000
		volume = boxReader.getShort(); // template: 0x0100
		boxReader.getShort(); // reserved
		boxReader.skip(8); // reserved

		boxReader.skip(9 * 4); // skip the 9 ints in the matrix
		boxReader.skip(6 * 4); // skip the "pre_defined", whatever it is
		nextTrackId = boxReader.getInteger();
	}

	public void setNextTrackId(int nextTrackId) {
		this.nextTrackId = nextTrackId;
	}

	@Override
	public int calculateSize() {
		int i = 0;
		if (version == 1) {
			i += 8 + 8 + 4 + 8;
		} else {
			i += 4 + 4 + 4 + 4;
		}
		i += 4 + 2 + 2 + 8 + (9 * 4) + (6 * 4) + 4;
		return super.calculateSize() + i;
	}

	@Override
	public void write(BoxWriter out) throws IOException {
		super.write(out);
		if (version == 1) {
			out.writeLong(creationTime);
			out.writeLong(modificationTime);
			out.writeInteger(timescale);
			out.writeLong(duration);
		} else {
			out.writeInteger((int) creationTime);
			out.writeInteger((int) modificationTime);
			out.writeInteger(timescale);
			out.writeInteger((int) duration);
		}
		out.writeInteger(rate);
		out.writeShort(volume);
		out.skip(2);
		out.skip(8);
		for (int i : matrix) {
			out.writeInteger(i);
		}
		for (int i : preDefined) {
			out.writeInteger(i);
		}
		out.writeInteger(nextTrackId);
	}

	@Override
	public String toString() {
		return super.toString() + "{" +
				"creationTime=" + creationTime +
				", modificationTime=" + modificationTime +
				", timescale=" + timescale +
				", duration=" + duration +
				", rate=" + rate +
				", volume=" + volume +
				", nextTrackId=" + nextTrackId +
				'}';
	}


}
