package net.jevring.mp4.boxes;

import net.jevring.mp4.BoxReader;
import net.jevring.mp4.BoxWriter;

import java.io.IOException;

/**
 * @author markus@jevring.net
 */
public class DataEntryBox extends FullBox {
	private final String name;
	private final String location;

	public DataEntryBox(int size, String type, BoxReader boxReader) throws IOException {
		super(size, type, null, false, boxReader);
		long startingOffset = boxReader.getOffset() - 8 - 4; // 8 for box, 4 for fullbox
		long expectedEndOffset = startingOffset + size;
		if ("urn ".equals(type) && boxReader.getOffset() < expectedEndOffset) {
			name = boxReader.getNullTerminatedString();
		} else {
			name = "";
		}
		if (boxReader.getOffset() < expectedEndOffset) {
			location = boxReader.getNullTerminatedString();
		} else {
			location = "";
		}
	}

	@Override
	public int calculateSize() {
		int nameSize = 0;
		int locationSize = 0;
		if ("urn ".equals(type)) {
			if (name.length() > 0) {
				nameSize = name.length() + 1;
			}
		}
		if (location.length() > 0) {
			locationSize = location.length() + 1;
		}
		return super.calculateSize() + nameSize + locationSize;
	}

	@Override
	public void write(BoxWriter out) throws IOException {
		super.write(out);
		if ("urn ".equals(type) && name.length() > 0) {
			out.writeNullTerminatedString(name);
		}
		if (location.length() > 0) {
			out.writeNullTerminatedString(location);
		}
	}

	@Override
	public String toString() {
		return super.toString() + "{" +
				"name='" + name + '\'' +
				", location='" + location + '\'' +
				'}';
	}
}
