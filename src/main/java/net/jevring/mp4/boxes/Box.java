package net.jevring.mp4.boxes;

import net.jevring.mp4.BoxWriter;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents either a basis for other boxes, or a box itself.
 *
 * @author markus@jevring.net
 */
public class Box {
	protected final int size;
	protected final String type;
	protected final Box container;
	protected final boolean canContainChildren;
	protected final List<Box> children = new ArrayList<>();

	public Box(int size, String type, Box container, boolean canContainChildren) {
		this.size = size;
		this.type = type;
		this.container = container;
		this.canContainChildren = canContainChildren;
		/*
				if (container != null) {
					System.out.println("Parent of '" + type + "' is '" + container.type + "'");
				} else {
					System.out.println("Parent of '" + type + "' is 'null'");
				}
				*/
		if (container != null) {
			// this leaks this, but this isn't a multi-threaded app, so we should be fine.
			container.children.add(this);
		}
	}

	public int getSize() {
		return size;
	}

	public String getType() {
		return type;
	}

	public Box getContainer() {
		return container;
	}

	public boolean canContainChildren() {
		return canContainChildren;
	}

	/**
	 * Writes the byte representation of this box and its children to the provided output stream.
	 *
	 * @param out the stream to write to
	 * @throws java.io.IOException if the write fails
	 */
	public void write(BoxWriter out) throws IOException {
		// todo: we can actually only write the size once we know how much more data we'll need to write.
		// we'll need to calculate that later somehow.
		// first, lets just see if we can successfully copy something using existing boxes
		// todo: actually, we have to do that now, since we have some partially parsed boxes.
		// we could simply skip them, I suppose. for now, at least
		out.writeInteger(calculateSize());
		out.write4ByteString(type);
	}

	public int calculateSize() {
		int s = 0;
		if (canContainChildren) {
			for (Box child : children) {
				s += child.calculateSize();
			}
		}
		return 8 + s;
	}

	// todo: if we want to do any authoring, we will have to come up with a nicer way of CREATING boxes.
	// one that doesn't require providing parent and children at time of creation

	public boolean disconnectFromParent() {
		return container.children.remove(this);
	}

	public List<Box> getChildren() {
		return children;
	}

	/**
	 * If there are multiple children of the same type, only returns the first.
	 *
	 * @param type the type of the child
	 * @return a child that has this type or nul if no such child exists
	 */
	public Box getChildOfType(String type) {
		for (Box child : children) {
			if (type.equals(child.type)) {
				return child;
			}
		}
		return null;
	}

	@Override
	public String toString() {
		String parentType = null;
		if (container != null) {
			parentType = container.type;
		}
		//return String.format("%25s{%4s,p=%4s, %10d, canContainChildren = %5s}", getClass().getSimpleName(), type, parentType, size, canContainChildren);
		return String.format("%25s{%4s,p=%4s, %10d}", getClass().getSimpleName(), type, parentType, size);
	}

	public void setChildOfType(String type, Box newChild) {
		Box childToReplace = null;
		for (Box child : children) {
			if (child.type.equals(type)) {
				childToReplace = child;
				break;
			}
		}
		if (childToReplace != null) {
			childToReplace.disconnectFromParent();
		}
		children.add(newChild);
	}
}
