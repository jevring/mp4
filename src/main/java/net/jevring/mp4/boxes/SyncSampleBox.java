package net.jevring.mp4.boxes;

import net.jevring.mp4.BoxReader;
import net.jevring.mp4.BoxWriter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author markus@jevring.net
 */
public class SyncSampleBox extends FullBox {
	private final int entryCount;
	private final List<Integer> sampleNumbers = new ArrayList<>();

	public SyncSampleBox(int size, Box container, BoxReader boxReader) throws IOException {
		super(size, "stss", container, false, boxReader);

		entryCount = boxReader.getInteger();
		for (int i = 0; i < entryCount; i++) {
			sampleNumbers.add(boxReader.getInteger());
		}
	}

	@Override
	public int calculateSize() {
		return super.calculateSize() + 4 + (entryCount * 4);
	}

	@Override
	public void write(BoxWriter out) throws IOException {
		super.write(out);
		out.writeInteger(entryCount);
		for (Integer sampleNumber : sampleNumbers) {
			out.writeInteger(sampleNumber);
		}
	}

	@Override
	public String toString() {
		return super.toString() + "{" +
				"entryCount=" + entryCount +
				//", sampleNumbers=<" + sampleNumbers.size() + ">" +
				", sampleNumbers=" + sampleNumbers +
				'}';
	}
}
