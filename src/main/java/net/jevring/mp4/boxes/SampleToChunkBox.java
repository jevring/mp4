package net.jevring.mp4.boxes;

import net.jevring.mp4.BoxReader;
import net.jevring.mp4.BoxWriter;
import net.jevring.mp4.Demuxer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author markus@jevring.net
 */
public class SampleToChunkBox extends FullBox {
	private final int entryCount;
	private final List<ChunkOffset> chunkOffsets = new ArrayList<>();

	public SampleToChunkBox(int size, Box container, BoxReader boxReader) throws IOException {
		super(size, "stsc", container, false, boxReader);

		entryCount = boxReader.getInteger();
		for (int i = 0; i < entryCount; i++) {
			chunkOffsets.add(new ChunkOffset(boxReader.getInteger(), boxReader.getInteger(), boxReader.getInteger()));
		}
	}

	public List<ChunkOffset> getChunkOffsets() {
		return chunkOffsets;
	}

	@Override
	public int calculateSize() {
		return super.calculateSize() + 4 + (entryCount * (3 * 4));
	}

	@Override
	public void write(BoxWriter out) throws IOException {
		super.write(out);
		out.writeInteger(entryCount);
		for (ChunkOffset chunkOffset : chunkOffsets) {
			out.writeInteger(chunkOffset.firstChunk);
			out.writeInteger(chunkOffset.samplesPerChunk);
			out.writeInteger(chunkOffset.sampleDescriptionIndex);
		}
	}

	@Override
	public String toString() {
		return super.toString() + "{" +
				"entryCount=" + entryCount +
				//", chunkOffsets=<" + chunkOffsets.size() + ">" +
				", chunkOffsets=" + chunkOffsets +
				'}';
	}

	public static class ChunkOffset {
		private final int firstChunk;
		private final int samplesPerChunk;
		private final int sampleDescriptionIndex;

		private ChunkOffset(int firstChunk, int samplesPerChunk, int sampleDescriptionIndex) {
			this.firstChunk = firstChunk;
			this.samplesPerChunk = samplesPerChunk;
			this.sampleDescriptionIndex = sampleDescriptionIndex;
		}

		public int getFirstChunk() {
			return firstChunk;
		}

		public int getSamplesPerChunk() {
			return samplesPerChunk;
		}

		public int getSampleDescriptionIndex() {
			return sampleDescriptionIndex;
		}

		@Override
		public String toString() {
			return "ChunkOffset{" +
					"firstChunk=" + firstChunk +
					", samplesPerChunk=" + samplesPerChunk +
					", sampleDescriptionIndex=" + sampleDescriptionIndex +
					'}';
		}
	}
}
