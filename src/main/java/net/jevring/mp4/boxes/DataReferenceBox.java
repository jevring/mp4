package net.jevring.mp4.boxes;

import net.jevring.mp4.BoxReader;
import net.jevring.mp4.BoxWriter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author markus@jevring.net
 */
public class DataReferenceBox extends FullBox {
	private final int entryCount;
	private final List<DataEntryBox> entries = new ArrayList<>();

	public DataReferenceBox(int size, Box container, BoxReader boxReader) throws IOException {
		super(size, "dref", container, false, boxReader);
		entryCount = boxReader.getInteger();
		for (int i = 0; i < entryCount; i++) {
			int entrySize = boxReader.getInteger();
			String entryType = boxReader.get4ByteString();
			DataEntryBox deb = new DataEntryBox(entrySize, entryType, boxReader);
			entries.add(deb);
		}
	}

	@Override
	public int calculateSize() {
		int i = 4;
		for (DataEntryBox entry : entries) {
			i += entry.calculateSize();
		}
		return super.calculateSize() + i;
	}

	@Override
	public void write(BoxWriter out) throws IOException {
		super.write(out);
		out.writeInteger(entryCount);
		for (DataEntryBox entry : entries) {
			entry.write(out);
		}
	}

	@Override
	public String toString() {
		return super.toString() + "{" +
				"entryCount=" + entryCount +
				", entries=" + entries +
				'}';
	}
}
