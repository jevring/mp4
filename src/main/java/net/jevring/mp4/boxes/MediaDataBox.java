package net.jevring.mp4.boxes;

import net.jevring.mp4.BoxReader;
import net.jevring.mp4.BoxWriter;

import java.io.IOException;

/**
 * @author markus@jevring.net
 */
public class MediaDataBox extends Box {

	public MediaDataBox(int size, Box container, BoxReader boxReader) throws IOException {
		super(size, "mdat", container, false);
		boxReader.skip(size - 8);
	}

	@Override
	public int calculateSize() {
		// todo: a better explanation might be good
		throw new UnsupportedOperationException("This is just a holder, stream this data out");
	}

	@Override
	public void write(BoxWriter out) throws IOException {
		// todo: a better explanation might be good
		throw new UnsupportedOperationException("This is just a holder, stream this data out");
	}
}
