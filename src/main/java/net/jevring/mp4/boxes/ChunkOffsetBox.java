package net.jevring.mp4.boxes;

import net.jevring.mp4.BoxReader;
import net.jevring.mp4.BoxWriter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author markus@jevring.net
 */
public class ChunkOffsetBox extends FullBox {
	private final int entryCount;
	private final List<Integer> chunkOffsets;

	public ChunkOffsetBox(int size, Box container, BoxReader boxReader) throws IOException {
		super(size, "stco", container, false, boxReader);

		entryCount = boxReader.getInteger();
		chunkOffsets = new ArrayList<>();
		for (int i = 0; i < entryCount; i++) {
			chunkOffsets.add(boxReader.getInteger());
		}
	}

	public ChunkOffsetBox(int size, Box container, int entryCount, List<Integer> chunkOffsets, int version, byte[] flags) {
		super(size, "stco", container, false, version, flags);
		this.entryCount = entryCount;
		this.chunkOffsets = chunkOffsets;
	}

	@Override
	public int calculateSize() {
		return super.calculateSize() + 4 + (entryCount * 4);
	}

	@Override
	public void write(BoxWriter out) throws IOException {
		super.write(out);
		out.writeInteger(entryCount);
		for (Integer integer : chunkOffsets) {
			out.writeInteger(integer);
		}
	}

	public int getEntryCount() {
		return entryCount;
	}

	public List<Integer> getChunkOffsets() {
		return chunkOffsets;
	}

	@Override
	public String toString() {
		return super.toString() + "{" +
				"entryCount=" + entryCount +
				//", chunkOffsets=<" + chunkOffsets.size() + ">" +
				", chunkOffsets=" + chunkOffsets +
				'}';
	}
}
