package net.jevring.mp4.boxes;

import net.jevring.mp4.BoxReader;
import net.jevring.mp4.BoxWriter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author markus@jevring.net
 */
public class SampleSizeBox extends FullBox {
	private final int sampleSize;
	private final int sampleCount;
	private final List<Integer> entrySizes = new ArrayList<>();

	public SampleSizeBox(int size, Box container, BoxReader boxReader) throws IOException {
		super(size, "stsz", container, false, boxReader);
		sampleSize = boxReader.getInteger();
		sampleCount = boxReader.getInteger();
		if (sampleSize == 0) {
			for (int i = 0; i < sampleCount; i++) {
				entrySizes.add(boxReader.getInteger());
			}
		}
	}

	public List<Integer> getEntrySizes() {
		return entrySizes;
	}

	@Override
	public int calculateSize() {
		return super.calculateSize() + 4 + 4 + (entrySizes.size() * 4);
	}

	@Override
	public void write(BoxWriter out) throws IOException {
		super.write(out);
		out.writeIntegers(sampleSize, sampleCount);
		for (Integer entrySize : entrySizes) {
			out.writeInteger(entrySize);
		}
	}

	@Override
	public String toString() {
		return super.toString() + "{" +
				"sampleSize=" + sampleSize +
				", sampleCount=" + sampleCount +
				//", entrySizes=<" + entrySizes.size() + ">" +
				", entrySizes=" + entrySizes +
				'}';
	}
}
