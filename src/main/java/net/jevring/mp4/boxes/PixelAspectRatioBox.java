package net.jevring.mp4.boxes;

import net.jevring.mp4.BoxReader;
import net.jevring.mp4.BoxWriter;

import java.io.IOException;

/**
 * @author markus@jevring.net
 */
public class PixelAspectRatioBox extends Box {
	private final int hSpacing;
	private final int vSpacing;

	public PixelAspectRatioBox(int size, Box container, BoxReader boxReader) throws IOException {
		super(size, "pasp", container, false);
		hSpacing = boxReader.getInteger();
		vSpacing = boxReader.getInteger();
	}

	@Override
	public int calculateSize() {
		return super.calculateSize() + 4 + 4;
	}

	@Override
	public void write(BoxWriter out) throws IOException {
		super.write(out);
		out.writeInteger(hSpacing);
		out.writeInteger(vSpacing);
	}

	@Override
	public String toString() {
		return super.toString() + "{" +
				"hSpacing=" + hSpacing +
				", vSpacing=" + vSpacing +
				'}';
	}
}
