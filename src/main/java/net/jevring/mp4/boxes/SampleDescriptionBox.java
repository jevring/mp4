package net.jevring.mp4.boxes;

import net.jevring.mp4.BoxReader;
import net.jevring.mp4.BoxWriter;
import net.jevring.mp4.boxes.sample.AudioSampleEntry;
import net.jevring.mp4.boxes.sample.SampleEntry;
import net.jevring.mp4.boxes.sample.VisualSampleEntry;
import net.jevring.mp4.boxes.sample.mp42.MP4AudioSampleEntry;
import net.jevring.mp4.boxes.sample.mp42.MP4VisualSampleEntry;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author markus@jevring.net
 */
public class SampleDescriptionBox extends FullBox {
	private final int entryCount;
	private final List<SampleEntry> entries = new ArrayList<>();

	public SampleDescriptionBox(int size, Box container, BoxReader boxReader) throws IOException {
		super(size, "stsd", container, false, boxReader);
		entryCount = boxReader.getInteger();
		String handlerType = getHandlerType();
		for (int i = 0; i < entryCount; i++) {
			int entrySize = boxReader.getInteger();
			String codingName = boxReader.get4ByteString();
			switch (handlerType) {
				case "soun":
					if ("mp4a".equals(codingName)) {
						entries.add(new MP4AudioSampleEntry(entrySize, boxReader));
					} else {
						entries.add(new AudioSampleEntry(entrySize, codingName, boxReader));
					}
					break;
				case "vide":
					if ("mp4v".equals(codingName)) {
						entries.add(new MP4VisualSampleEntry(entrySize, boxReader));
					} else {
						entries.add(new VisualSampleEntry(entrySize, codingName, boxReader));
					}
					break;
				case "hint":
					entries.add(new SampleEntry(entrySize, codingName, boxReader) {
					});
					break;
				case "meta":
					entries.add(new SampleEntry(entrySize, codingName, boxReader) {
					});
					break;
			}
		}
	}

	private String getHandlerType() {
		Box b = container;
		while ((b = b.container) != null) {
			//System.out.println("Parent: " + b.type);
			if ("mdia".equals(b.type)) {
				// we've gone to the parent of what we want. look in the list of children
				for (Box child : b.children) {
					if ("hdlr".equals(child.type)) {
						return ((HandlerBox) child).getHandlerType();
					}
				}
			}
		}
		throw new IllegalStateException("Box type 'stsd' did not have a 'hdlr' parent");
	}

	@Override
	public void write(BoxWriter out) throws IOException {
		super.write(out);
		out.writeInteger(entryCount);
		for (SampleEntry entry : entries) {
			entry.write(out);
		}

	}

	@Override
	public int calculateSize() {
		int s = 4; // entry count int
		for (SampleEntry entry : entries) {
			s += entry.calculateSize();
		}
		return super.calculateSize() + s;
	}

	@Override
	public String toString() {
		return super.toString() + "{" +
				"entryCount=" + entryCount +
				", entries=" + entries +
				'}';
	}
}
