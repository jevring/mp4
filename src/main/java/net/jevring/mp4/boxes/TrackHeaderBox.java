package net.jevring.mp4.boxes;

import net.jevring.mp4.BoxReader;
import net.jevring.mp4.BoxWriter;

import java.io.IOException;

/**
 * @author markus@jevring.net
 */
public class TrackHeaderBox extends FullBox {
	public static final int TRACK_ENABLED = 0x000001;
	public static final int TRACK_IN_MOVIE = 0x000002;
	public static final int TRACK_IN_PREVIEW = 0x000004;
	private final long creationTime;
	private final long modificationTime;
	private int trackId;
	private final long duration;
	private final short layer;
	private final short alternateGroup;
	private final short volume; // if track_is_audio 0x0100 else 0
	private final int[] matrix = new int[]{0x00010000, 0, 0, 0, 0x00010000, 0, 0, 0, 0x40000000};
	private final int width;
	private final int height;

	public TrackHeaderBox(int size, Box container, BoxReader boxReader) throws IOException {
		super(size, "tkhd", container, false, boxReader);
		if (version == 1) {
			creationTime = boxReader.getLong();
			modificationTime = boxReader.getLong();
			trackId = boxReader.getInteger();
			boxReader.getInteger(); // reserved
			duration = boxReader.getLong();
		} else {
			creationTime = boxReader.getInteger();
			modificationTime = boxReader.getInteger();
			trackId = boxReader.getInteger();
			boxReader.getInteger(); // reserved
			duration = boxReader.getInteger();
		}
		boxReader.getInteger(); // reserved
		boxReader.getInteger(); // reserved
		layer = boxReader.getShort();
		alternateGroup = boxReader.getShort();
		volume = boxReader.getShort();
		boxReader.getShort(); // reserved
		boxReader.skip(9 * 4); // skip the 9 ints in the matrix
		width = boxReader.getInteger();
		height = boxReader.getInteger();
	}

	public int getTrackId() {
		return trackId;
	}

	public void setTrackId(int trackId) {
		this.trackId = trackId;
	}

	@Override
	public int calculateSize() {
		int i = 0;
		if (version == 1) {
			i += 8 + 8 + 4 + 4 + 8;
		} else {
			i += 4 + 4 + 4 + 4 + 4;
		}
		i += 4 + 4 + 2 + 2 + 2 + 2 + (9 * 4) + 4 + 4;
		return super.calculateSize() + i;
	}

	@Override
	public void write(BoxWriter out) throws IOException {
		super.write(out);
		if (version == 1) {
			out.writeLong(creationTime);
			out.writeLong(modificationTime);
			out.writeInteger(trackId);
			out.skip(4);
			out.writeLong(duration);
		} else {
			out.writeInteger((int) creationTime);
			out.writeInteger((int) modificationTime);
			out.writeInteger(trackId);
			out.skip(4);
			out.writeInteger((int) duration);
		}
		out.skip(8);
		out.writeShort(layer);
		out.writeShort(alternateGroup);
		out.writeShort(volume);
		out.skip(2);
		for (int i : matrix) {
			out.writeInteger(i);
		}
		out.writeInteger(width);
		out.writeInteger(height);
	}

	@Override
	public String toString() {
		return super.toString() + "{" +
				"creationTime=" + creationTime +
				", modificationTime=" + modificationTime +
				", trackId=" + trackId +
				", duration=" + duration +
				", layer=" + layer +
				", alternateGroup=" + alternateGroup +
				", volume=" + volume +
				", width=" + width +
				", height=" + height +
				'}';
	}
}
