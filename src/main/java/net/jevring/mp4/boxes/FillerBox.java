package net.jevring.mp4.boxes;

import net.jevring.mp4.BoxReader;
import net.jevring.mp4.BoxWriter;

import java.io.IOException;

/**
 * @author markus@jevring.net
 */
public class FillerBox extends Box {
	private final byte[] data;

	public FillerBox(int size, String type, Box container, BoxReader boxReader) throws IOException {
		super(size, type, container, false);
		data = boxReader.getBytes(size - 8);
	}

	@Override
	public int calculateSize() {
		return super.calculateSize() + data.length;
	}

	@Override
	public void write(BoxWriter out) throws IOException {
		super.write(out);
		out.writeBytes(data);
	}
}
