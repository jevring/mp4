package net.jevring.mp4.boxes;

import net.jevring.mp4.BoxReader;

import java.io.IOException;

/**
 * @author markus@jevring.net
 */
public class ObjectDescriptorBox extends FullBox {
	// note: this is part of mp42
	// todo: javadoc what all the boxes are/do


	public ObjectDescriptorBox(int size, Box container, BoxReader boxReader) throws IOException {
		super(size, "iods", container, false, boxReader);
		// -8 for size + type
		// -4 for FullBox
		long startingOffset = boxReader.getOffset() - 8 - 4;
		long targetOffset = startingOffset + size;
		//System.out.println("Bytes left: " + (targetOffset - boxReader.getOffset()));
		//System.out.println("boxReader.getString((int) (targetOffset - boxReader.getOffset())) = " + boxReader.getString((int) (targetOffset - boxReader.getOffset())));
	}
}
