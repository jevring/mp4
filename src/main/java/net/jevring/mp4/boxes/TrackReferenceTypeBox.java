package net.jevring.mp4.boxes;

import net.jevring.mp4.BoxReader;
import net.jevring.mp4.BoxWriter;

import java.io.IOException;
import java.util.Arrays;

/**
 * @author markus@jevring.net
 */
public class TrackReferenceTypeBox extends Box {
	private final int[] trackIds;

	public TrackReferenceTypeBox(int size, Box container, BoxReader boxReader) throws IOException {
		// todo: there's more to do here, check 8.3.3.3
		super(size, "tref", container, false);
		int numberOfTrackReferences = (int) ((size - boxReader.getOffset()) / 4);

		trackIds = new int[numberOfTrackReferences];
		for (int i = 0; i < numberOfTrackReferences; i++) {
			trackIds[i] = boxReader.getInteger();
		}
	}

	@Override
	public int calculateSize() {
		return super.calculateSize() + (trackIds.length * 4);
	}

	@Override
	public void write(BoxWriter out) throws IOException {
		super.write(out);
		for (int trackId : trackIds) {
			out.writeInteger(trackId);
		}
	}

	@Override
	public String toString() {
		return super.toString() + Arrays.toString(trackIds);
	}
}
