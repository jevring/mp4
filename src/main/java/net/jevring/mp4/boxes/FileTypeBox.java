package net.jevring.mp4.boxes;

import net.jevring.mp4.BoxReader;
import net.jevring.mp4.BoxWriter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author markus@jevring.net
 */
public class FileTypeBox extends Box {
	private String majorBrand;
	private int minorVersion;
	private final List<String> compatibleBrands = new ArrayList<>();

	public FileTypeBox(int size, Box container, BoxReader boxReader) throws IOException {
		super(size, "ftyp", container, false);
		majorBrand = boxReader.get4ByteString();
		minorVersion = boxReader.getInteger();
		int numberOfCompatibleBrands = (int) ((size - boxReader.getOffset()) / 4);

		for (int i = 0; i < numberOfCompatibleBrands; i++) {
			compatibleBrands.add(boxReader.get4ByteString());
		}
	}

	@Override
	public int calculateSize() {
		return super.calculateSize() + 4 + 4 + (compatibleBrands.size() * 4);
	}

	@Override
	public void write(BoxWriter out) throws IOException {
		super.write(out);
		out.write4ByteString(majorBrand);
		out.writeInteger(minorVersion);
		for (String compatibleBrand : compatibleBrands) {
			out.write4ByteString(compatibleBrand);
		}
	}

	@Override
	public String toString() {
		return super.toString() + "{" +
				"majorBrand='" + majorBrand + '\'' +
				", minorVersion=" + minorVersion +
				", compatibleBrands=" + compatibleBrands +
				'}';
	}

	public void setMajorBrand(String majorBrand) {
		this.majorBrand = majorBrand;
	}

	public void setCompatibleBrands(String... compatibleBrands) {
		this.compatibleBrands.clear();
		Collections.addAll(this.compatibleBrands, compatibleBrands);
	}

	public void setMinorVersion(int minorVersion) {
		this.minorVersion = minorVersion;
	}

}
