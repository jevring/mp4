package net.jevring.mp4;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;

/**
 * @author markus@jevring.net
 */
public class BoxReader {
	private final byte[] bytes = new byte[4];
	private long offset = 0;
	private final DataInputStream input;

	public BoxReader(InputStream input) {
		// todo: strangely, we get an exception if we wrap it in a BufferedInputStream. why?
		//if (input instanceof BufferedInputStream) {
		this.input = new DataInputStream(input);
		//} else {
		//    this.input = new DataInputStream(new BufferedInputStream(input));
		//}
	}

	public String get4ByteString() throws IOException {
		offset += input.read(bytes);
		return new String(bytes);
	}

	public String getString(int length) throws IOException {
		return new String(getBytes(length));
	}

	public int getInteger() throws IOException {
		offset += 4;
		return input.readInt();
	}

	public long getUnsignedInteger() throws IOException {
		return getInteger();
		// return getInteger(); // 19 minutes
		/* 19 minuses
				byte[] bytes = getBytes(4);
				return new BigInteger(bytes).longValue();
				*/
		// 17 minuses
		/* this returns positively weird numbers for everything, so lets skip it until it's needed
				long l = 0;
				for (byte b : bytes) {
					l |= b & 0xFF; // add the bits
					l <<= 8; // push them to the proper location
				}
				return l;
				*/
	}

	public short getShort() throws IOException {
		offset += 2;
		return input.readShort();
	}

	public int getSingleByteInteger() throws IOException {
		offset += 1;
		return input.read();
	}

	public long getLong() throws IOException {
		offset += 8;
		return input.readLong();
	}

	public long getOffset() {
		return offset;
	}

	public long skip(long desiredSkipLength) throws IOException {
		final long skippedBytes = input.skip(desiredSkipLength);
		offset += skippedBytes;
		return skippedBytes;
	}

	public byte[] getBytes(int n) throws IOException {
		byte[] b = new byte[n];
		offset += input.read(b);
		return b;
	}

	public String getNullTerminatedString() throws IOException {
		StringBuilder sb = new StringBuilder();
		while (true) {
			byte b = input.readByte();
			offset++;
			//System.out.println("b = " + b + " as char " + (char)b);
			if (b == 0x0) {
				break;
			}
			sb.append((char) b);
		}
		return sb.toString();
	}


	public String getLengthPrefixedString(int maxLength) throws IOException {
		long startingOffset = getOffset();
		long targetOffset = startingOffset + maxLength;

		int length = getShort();
		String s = getString(length);
		long remainingBytesUntilMaxLength = targetOffset - getOffset();
		skip(remainingBytesUntilMaxLength);
		return s;
	}
}
