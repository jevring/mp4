package net.jevring.mp4;

import net.jevring.mp4.boxes.Box;
import net.jevring.mp4.boxes.MP4File;

/**
 * @author markus@jevring.net
 */
public class Printer {
	public void print(MP4File file) {
		int deepestDepth = 0;
		for (Box box : file.getBoxes()) {
			deepestDepth = Math.max(depthOf(box, 0), deepestDepth);
			//deepestDepth = Math.max(deepestDepth, deepestDepth(box, 0));
		}

		System.out.println("deepestDepth = " + deepestDepth);
		final String format = "%-" + (deepestDepth + 2) * 4 + "s%s %s\n";


		for (Box box : file.getBoxes()) {
			System.out.printf(format, indent(depthOf(box, 0)) + box.getType(), "|", box.toString());
			//printBoxes(box, format);
		}
	}

	private void printBoxes(Box box, String format) {
		System.out.printf(format, indent(depthOf(box, 0)) + box.getType(), "|", box.toString());
		for (Box child : box.getChildren()) {
			printBoxes(child, format);
		}
	}

	private int deepestDepth(Box box, int depth) {
		int deepestDepth = depth;
		for (Box child : box.getChildren()) {
			deepestDepth = Math.max(deepestDepth, deepestDepth(child, depth + 1));
		}
		return deepestDepth;
	}


	private String indent(int n) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < n; i++) {
			sb.append("    ");
		}
		return sb.toString();
	}

	private int depthOf(Box box, int depth) {
		if (box.getContainer() != null) {
			return depthOf(box.getContainer(), depth + 1);
		} else {
			return depth;
		}
	}
}
