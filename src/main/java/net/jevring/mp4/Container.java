package net.jevring.mp4;

import net.jevring.mp4.boxes.Box;

/**
 * @author markus@jevring.net
 */
public class Container extends Box {
	public Container(int size, String type, Box container) {
		super(size, type, container, true);
	}

	// todo: make Box abstract, and ensure this is the only thing used as a container.
	// todo: get rid of the boolean
}
