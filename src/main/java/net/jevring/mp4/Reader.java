package net.jevring.mp4;

import net.jevring.mp4.boxes.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author markus@jevring.net
 */
public class Reader {
	private final Map<Box, Long> startingOffsets = new HashMap<>();
	private final List<Box> boxes = new ArrayList<>();
	private final InputStream in;
	private final MP4File mp4File;

	public Reader(InputStream in, MP4File mp4File) {
		this.in = in;
		this.mp4File = mp4File;
	}

	public void read() throws IOException {
		startingOffsets.put(mp4File, 0L);
		Box container = mp4File;
		BoxReader boxReader = new BoxReader(in);
		while (in.available() > 0) {
			Box box = getBox(boxReader, container);

			if (box.canContainChildren()) {
				container = box;
			} else {
				Box b = box;
				if (isLastInParent(b, mp4File)) {
					//System.out.println(b.getType() +  " is last in parent " + b.getContainer().getType());
					while (isLastInParent(b, mp4File)) {
						//System.out.println("-- " + b.getType() +  " is last in parent " + b.getContainer().getType());
						b = b.getContainer();
					}
					container = b.getContainer();
				}
			}
		}
	}

	private boolean isLastInParent(Box box, MP4File file) {
		Box parent = box.getContainer();
		if (parent == file) {
			//System.out.println(box.getType() + " has no parent");
			return false;
		}
		long startingOffsetOfParent = startingOffsets.get(parent);
		long endOfParent = startingOffsetOfParent + parent.getSize();
		long startingOffsetOfChild = startingOffsets.get(box);
		return endOfParent <= startingOffsetOfChild + box.getSize();
	}

	private Box getBox(BoxReader boxReader, Box container) throws IOException {
		long startingOffset = boxReader.getOffset(); // we use this to skip in case there are some header values we can't/don't want to read
		int size;
		String boxType;
		long largeSize = 0;
		int userType = 0;
		size = boxReader.getInteger();
		boxType = boxReader.get4ByteString();
		if (size == 1) {
			largeSize = boxReader.getLong();
		}

		Box box;

		// todo: all the integers are unsigned. we need to read the right thing
		switch (boxType) {
			case "ftyp":
				box = new FileTypeBox(size, container, boxReader);
				break;
			case "moov":
				// parent=File
				box = new Box(size, boxType, container, true);
				break;
			case "mvhd":
				// parent=moov
				box = new MovieHeaderBox(size, container, boxReader);
				break;
			case "iods":
				// object description
				// parent=moov
				//box = new ObjectDescriptorBox(size, container, boxReader);
				box = new FillerBox(size, boxType, container, boxReader);
				break;
			case "trak":
				// parent=moov
				box = new TrackBox(size, container);
				break;
			case "tkhd":
				// parent=trak
				box = new TrackHeaderBox(size, container, boxReader);
				break;
			case "tref":
				// parent=trak
				box = new TrackReferenceTypeBox(size, container, boxReader);
				break;
			case "edts":
				// parent=trak
				box = new Box(size, boxType, container, false);
				break;
			case "mdia":
				// parent=trak
				box = new Box(size, boxType, container, true);
				break;
			case "mdhd":
				// parent=mdia
				box = new MediaHeaderBox(size, container, boxReader);
				break;
			case "hdlr":
				// parent=mdia
				box = new HandlerBox(size, container, boxReader);
				break;
			case "minf":
				// parent=mdia
				box = new Box(size, boxType, container, true);
				break;
			case "dinf":
				// parent=minf
				box = new Box(size, boxType, container, true);
				break;
			case "stbl":
				// parent=minf
				box = new Box(size, boxType, container, true);
				break;
			case "stdp":
				// parent=stbl
				box = new FullBox(size, boxType, container, false, boxReader);
				break;
			case "stts":
				// parent=stbl
				box = new TimeToSampleBox(size, container, boxReader);
				break;
			case "ctts":
				// parent=stbl
				box = new FullBox(size, boxType, container, false, boxReader);
				break;
			case "stsd":
				// parent=stbl
				box = new SampleDescriptionBox(size, container, boxReader);
				break;
			case "stss":
				// parent=stbl
				box = new SyncSampleBox(size, container, boxReader);
				break;
			case "stsh":
				// parent=stbl
				box = new FullBox(size, boxType, container, false, boxReader);
				break;
			case "stsc":
				// parent=stbl
				box = new SampleToChunkBox(size, container, boxReader);
				break;
			case "stco":
				// parent=stbl
				box = new ChunkOffsetBox(size, container, boxReader);
				break;
			case "co64":
				// parent=stbl
				box = new FullBox(size, boxType, container, false, boxReader);
				break;
			case "padb":
				// parent=stbl
				box = new FullBox(size, boxType, container, false, boxReader);
				break;
			case "stsz":
				// parent=stbl
				box = new SampleSizeBox(size, container, boxReader);
				break;
			case "stz2":
				// parent=stbl
				box = new CompactSampleSizeBox(size, container, boxReader);
				break;
			case "sdtp":
				// parent=stbl, traf
				box = new FullBox(size, boxType, container, false, boxReader);
				break;
			case "subs":
				// parent=stbl, traf
				box = new FullBox(size, boxType, container, false, boxReader);
				break;
			case "sbgp":
				// parent=stbl, traf
				box = new FullBox(size, boxType, container, false, boxReader);
				break;
			case "tfhd":
				// parent=traf
				box = new FullBox(size, boxType, container, false, boxReader);
				break;
			case "trun":
				// parent=traf
				box = new FullBox(size, boxType, container, false, boxReader);
				break;
			case "mehd":
				// parent=mvex
				box = new FullBox(size, boxType, container, false, boxReader);
				break;
			case "trex":
				// parent=mvex
				box = new FullBox(size, boxType, container, false, boxReader);
				break;
			case "elst":
				// parent=edts
				// mandatory=yes
				// quantity=1
				box = new FullBox(size, boxType, container, false, boxReader);
				break;
			case "url":
			case "urln":
				// parent=dinf
				box = new FullBox(size, boxType, container, false, boxReader);
				break;
			case "dref":
				// parent=dinf
				box = new DataReferenceBox(size, container, boxReader);
				break;
			case "vmhd":
				// parent=minf
				box = new VideoMediaHeaderBox(size, container, boxReader);
				break;
			case "smhd":
				// parent=minf
				box = new SoundMediaHeaderBox(size, container, boxReader);
				break;
			case "hmhd":
				// parent=minf
				// mandatory=yes
				// quantity=1
				box = new FullBox(size, boxType, container, false, boxReader);
				break;
			case "nmhd":
				// parent=minf
				// mandatory=yes
				// quantity=1
				box = new FullBox(size, boxType, container, false, boxReader);
				break;
			case "mdat":
				// parent=moov
				box = new MediaDataBox(size, container, boxReader);
				break;
			case "mvex":
				// parent=moov
				box = new Box(size, boxType, container, true);
				break;
			case "moof":
				// parent=File
				box = new Box(size, boxType, container, true);
				break;
			case "traf":
				// parent=moof
				box = new Box(size, boxType, container, true);
				break;
			case "mfhd":
				// parent=moof
				box = new FullBox(size, boxType, container, false, boxReader);
				break;
			case "mfra":
				// parent=File
				box = new Box(size, boxType, container, true);
				break;
			case "tfra":
				// parent=mfra
				box = new FullBox(size, boxType, container, false, boxReader);
				break;
			case "mfro":
				// parent=mfra
				box = new FullBox(size, boxType, container, false, boxReader);
				break;
			case "meta":
				// parent=moov, trak, meco
				box = new FullBox(size, boxType, container, true, boxReader);
				break;
			case "udta":
				// parent=moov, trak
				box = new Box(size, boxType, container, true);
				break;
			case "cprt":
				// parent=udta
				box = new FullBox(size, boxType, container, false, boxReader);
				break;
			case "tsel":
				// parent=udta
				box = new FullBox(size, boxType, container, false, boxReader);
				break;
			case "ilst":
				// parent=udta
				box = new FillerBox(size, boxType, container, boxReader);
				break;
			case "fiin":
				// parent=meta
				box = new FullBox(size, boxType, container, true, boxReader);
				break;
			case "gitn":
				// parent=fiin
				box = new FullBox(size, boxType, container, false, boxReader);
				break;
			case "srpp":
				// parent=???
				box = new FullBox(size, boxType, container, false, boxReader);
				break;
			case "fpar":
				// parent=paen
				box = new FullBox(size, boxType, container, false, boxReader);
				break;
			case "fecr":
				// parent=paen
				box = new FullBox(size, boxType, container, false, boxReader);
				break;
			case "xml":
			case "bxml":
				// parent=meta
				box = new FullBox(size, boxType, container, false, boxReader);
				break;
			case "iloc":
				// parent=meta
				box = new FullBox(size, boxType, container, false, boxReader);
				break;
			case "pitm":
				// parent=meta
				box = new FullBox(size, boxType, container, false, boxReader);
				break;
			case "paen":
				// parent=meta
				box = new Box(size, boxType, container, true);
				break;
			case "ipro":
				// parent=meta
				box = new FullBox(size, boxType, container, true, boxReader);
				break;
			case "iinf":
				// parent=meta
				box = new FullBox(size, boxType, container, false, boxReader);
				break;
			case "sinf":
				// parent=ipro
				box = new Box(size, boxType, container, true);
				break;
			case "meco":
				// parent=File
				box = new Box(size, boxType, container, true);
				break;
			case "mere":
				// parent=meco
				box = new FullBox(size, boxType, container, false, boxReader);
				break;
			case "imif":
				// parent=sing
				box = new FullBox(size, boxType, container, false, boxReader);
				break;
			case "pdin":
				// parent=File
				box = new ProgressiveDownloadInfoBox(size, container, boxReader);
				break;
			case "ipmc":
				// parent=moov, meta
				box = new FullBox(size, boxType, container, false, boxReader);
				break;
			case "schm":
				// parent=sinf, srpp
				box = new FullBox(size, boxType, container, false, boxReader);
				break;
			case "free":
			case "skip":
				// parent=File
				box = new Box(size, boxType, container, true);
				break;
			default:
				box = new Box(size, boxType, container, false);
				//throw new IllegalArgumentException("Unknown box type: " + boxType);
		}

		// how can I know where compatibleBrands ends? How do I know where the box ends?
		// if size is 0, then it...
		// UNLESS the size is actually the size, and the boxes aren't contained WITHIN each other.

		if (!box.canContainChildren()) {
			long targetOffset = startingOffset + size;
			final long desiredSkipLength = targetOffset - boxReader.getOffset();
			//final String skippedContent = boxReader.getString((int) desiredSkipLength);

			long skipped = boxReader.skip(desiredSkipLength);
			if (desiredSkipLength != 0) {
				// todo: this must NEVER be < 0
				System.out.println("Desired skip length (" + boxType + "): " + desiredSkipLength);
				//System.out.println("skipped content = " + skippedContent);
			}
			//System.out.println("Skip length difference: " + (desiredSkipLength - skipped));
			//offset = targetOffset;
		}

		startingOffsets.put(box, startingOffset);
		return box;

	}

	public List<Box> getBoxes() {
		return boxes;
	}
}
