package net.jevring.mp4;

import net.jevring.mp4.boxes.MP4File;

/**
 * @author markus@jevring.net
 */
public interface Filter {
	void filter(MP4File file);
}
