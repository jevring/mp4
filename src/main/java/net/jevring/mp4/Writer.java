package net.jevring.mp4;

import net.jevring.mp4.boxes.Box;
import net.jevring.mp4.boxes.DataReferenceBox;
import net.jevring.mp4.boxes.ObjectDescriptorBox;
import net.jevring.mp4.boxes.TrackHeaderBox;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * @author markus@jevring.net
 */
public class Writer {
	private final BoxWriter out;

	public Writer(OutputStream out) {
		this.out = new BoxWriter(out);
	}

	public void write(Collection<Box> boxes) throws IOException {
		for (Box box : boxes) {
			box.write(out);
		}
	}
}
