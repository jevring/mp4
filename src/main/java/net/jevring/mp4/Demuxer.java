package net.jevring.mp4;

import net.jevring.mp4.boxes.*;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author markus@jevring.net
 */
public class Demuxer {
	private final MP4File source;
	private final TrackBox track;

	public Demuxer(MP4File source, TrackBox track) {
		this.source = source;
		this.track = track;
	}

	public void write(BoxWriter boxWriter) throws IOException {
		/*
		Take the boxes we need from the source file
		Take the boxes we need from the track
		Create a new stco box, insert it into the track
		determine mdat offset
		write new mdat using the source file and the new stco
		 */


		MediaDataBox mdat = (MediaDataBox) source.getChildOfType("mdat");
		mdat.disconnectFromParent();
		// calculate this here before we start gutting the original box structure
		long originalMediaDataBoxOffset = source.calculateSize();

		Box moov = source.getChildOfType("moov");
		((MovieHeaderBox)moov.getChildOfType("mvhd")).setNextTrackId(2);
		for (TrackBox track : source.getTracks()) {
			if (track != this.track) {
				track.disconnectFromParent();
			}
		}


		// ----------- grasping at straws -----------------------
		// todo: null check
		moov.getChildOfType("iods").disconnectFromParent();

		/*
		FileTypeBox ftyp = (FileTypeBox)source.getChildOfType("ftyp");
		ftyp.setMajorBrand("mp41"); // todo: perhaps try just isom
		ftyp.setCompatibleBrands("mp41","isom");
          */


		// HMM, WHEN DEMUXING THE VIDEO TRACK, IT RUNS FOR ABOUT 2 SECONDS, THEN SOME ERROR OCCURS.
		// presumably it's the same for sound, but we don't notice it.

		// ----------- grasping at straws -----------------------

		List<Chunk> chunks = getChunks();

		// box + fullbox + entryCount + entries
		int stcoSize = 8 + 4 + 4 + (chunks.size() * 4);
		final ChunkOffsetBox chunkOffsetBox = track.getChunkOffsetBox();
		List<Integer> chunkOffsets = new ArrayList<>();
		final ChunkOffsetBox newChunkOffsetBox = new ChunkOffsetBox(stcoSize,
		                                                          null,
		                                                          chunks.size(),
		                                                          chunkOffsets,
		                                                          chunkOffsetBox.getVersion(),
		                                                          chunkOffsetBox.getFlags());

		this.track.setChunkOffsetBox(newChunkOffsetBox);


		long sizeWithoutMediaDataBox = source.calculateSize();

		// todo: this also seems to be 8 bytes off. it's like we're accidentally including the mdat header box
		System.out.println("sizeWithoutMediaDataBox = " + sizeWithoutMediaDataBox);

		// todo: this actually points to the first original chunk offset. perhaps off by 8?
		System.out.println("originalMediaDataBoxOffset = " + originalMediaDataBoxOffset);

		// ----------- grasping at straws -----------------------
/*
		FileTypeBox ftyp = (FileTypeBox)source.getChildOfType("ftyp");
		ftyp.setMajorBrand("isom"); // todo: perhaps try just isom
		ftyp.setCompatibleBrands("isom");
		ftyp.setMinorVersion(1);
		ftyp.write(boxWriter);
*/

		// ----------- grasping at straws -----------------------

		writeNewChunkOffsets(sizeWithoutMediaDataBox, originalMediaDataBoxOffset, chunks, chunkOffsets);

		// first write the other stuff. after this, we write the mdat
		for (Box box : source.getBoxes()) {
			box.write(boxWriter);
		}


		writeNewMediaDataBox(chunks, boxWriter);
	}

	private void writeNewMediaDataBox(List<Chunk> chunks, BoxWriter boxWriter) throws IOException {
		int mdatSize = 8; // include size and type
		for (Chunk chunk : chunks) {
			mdatSize += chunk.chunkSize;
		}
		System.out.println("mdatSize = " + mdatSize);
		boxWriter.writeInteger(mdatSize);
		boxWriter.write4ByteString("mdat");

		long totalSizeWritten = 0;
		try (FileInputStream fis = new FileInputStream(source.getPath().toFile())) {
			long currentOffset = 0;
			for (Chunk chunk : chunks) {
				final long fromHereToNextChunkOffset = chunk.chunkOffset - currentOffset;
				System.out.println("fromHereToNextChunkOffset = " + fromHereToNextChunkOffset);
				long skipped = fis.skip(fromHereToNextChunkOffset);
				assert skipped == fromHereToNextChunkOffset;
				currentOffset = chunk.chunkOffset;
				byte[] b = new byte[chunk.chunkSize];
				int read = fis.read(b);
				boxWriter.writeBytes(b);
				totalSizeWritten += chunk.chunkSize;
			}
		}
		System.out.println("totalSizeWritten = " + totalSizeWritten);
		System.out.println("boxWriter.getOffset() = " + boxWriter.getOffset());

	}

	/**
	 * Writes new chunk offsets into the list that has already been passed to the new {@link ChunkOffsetBox}.
	 *
	 * @param baseOffset the base offset in the file where the new mdat box will start
	 * @param originalBaseOffset how far offset the mdat box was in the original file
	 * @param chunks the chunks from the previous stco
	 * @param chunkOffsets the list to write to. This list <b>MUST</b> be the list used when creating the new {@link ChunkOffsetBox}.
	 */
	private void writeNewChunkOffsets(long baseOffset, long originalBaseOffset, List<Chunk> chunks, List<Integer> chunkOffsets) {
		int lastChunkOffset = (int) baseOffset; // _todo +8? (tried, and it doesn't seem to help)
		for (Chunk chunk : chunks) {
			chunkOffsets.add(lastChunkOffset);
			lastChunkOffset += chunk.chunkSize;
		}
	}

	private List<Chunk> getChunks() {
		final SampleToChunkBox sampleToChunkBox = track.getSampleToChunkBox();
		final SampleSizeBox sampleSizeBox = track.getSampleSizeBox();
		final ChunkOffsetBox chunkOffsetBox = track.getChunkOffsetBox();
		List<Chunk> chunks = new ArrayList<>();
		
		int lastChunkOffsetEntry = 0;
		int lastSample = 0;
		final List<SampleToChunkBox.ChunkOffset> chunkOffsets = sampleToChunkBox.getChunkOffsets();
		
		//int expectedBytesRead = sum(sampleSizeBox.getEntrySizes(), 0, sampleSizeBox.getEntrySizes().size());
		//System.out.println("expectedBytesRead = " + expectedBytesRead);

		int totalChunkSize = 0;
		for (int i = 0; i < chunkOffsetBox.getChunkOffsets().size(); i++) {
			Integer chunkOffset = chunkOffsetBox.getChunkOffsets().get(i);

			// calculate chunk size
			SampleToChunkBox.ChunkOffset lastChunkOffset = chunkOffsets.get(lastChunkOffsetEntry);
			if (lastChunkOffsetEntry < chunkOffsets.size()) {
				SampleToChunkBox.ChunkOffset nextChunkOffset = chunkOffsets.get(lastChunkOffsetEntry + 1);
				if (nextChunkOffset.getFirstChunk() == i + 1) {
					lastChunkOffset = nextChunkOffset;
					lastChunkOffsetEntry++;
				} // else leave the lastChunkOffset to whatever it was before
			}
			int chunkSize = sum(sampleSizeBox.getEntrySizes(), lastSample, lastChunkOffset.getSamplesPerChunk());
			totalChunkSize += chunkSize;
			//System.out.println("chunkSize = " + chunkSize);
			chunks.add(new Chunk(chunkOffset, chunkSize));
			lastSample += lastChunkOffset.getSamplesPerChunk();

			// todo: either the chunk size is wrong when we sum it or when we read it, because we end up with offsets that are WAY outside the file length
			// reading it looks fine, as the total chunk size equals the number of bytes written.
			// it must be, then, the calculation of the chunkOffset list

			// if the firstChunk of the next ChunkOffset is equal to our chunk offset, treat this chunk then advance to the next ChunkOffset
			 
			
		}

		System.out.println("totalChunkSize = " + totalChunkSize);

		return chunks;
	}

	private List<Chunk> getChunksx() {
		final SampleToChunkBox sampleToChunkBox = track.getSampleToChunkBox();
		final SampleSizeBox sampleSizeBox = track.getSampleSizeBox();
		final ChunkOffsetBox chunkOffsetBox = track.getChunkOffsetBox();

		System.out.println("sampleSizeBox.getEntrySizes() = " + sampleSizeBox.getEntrySizes().size());

		List<Chunk> chunks = new ArrayList<>();

		long expectedMediaDataLength = sum(sampleSizeBox.getEntrySizes(), 0, sampleSizeBox.getEntrySizes().size());
		
		int lastSample = 0;
		for (SampleToChunkBox.ChunkOffset chunk : sampleToChunkBox.getChunkOffsets()) {
			// since we keep these in an array list, the chunk lookup is O(1)
			int chunkOffset = chunkOffsetBox.getChunkOffsets().get(chunk.getFirstChunk() - 1);
			// todo: deal with the case with a single sample size
			int chunkSize = sum(sampleSizeBox.getEntrySizes(), lastSample, chunk.getSamplesPerChunk());
			System.out.println("chunkSize = " + chunkSize);
			lastSample += chunk.getSamplesPerChunk();
			int chunkEnd = chunkOffset + chunkSize;
			chunks.add(new Chunk(chunkOffset, chunkSize));
		}
		System.out.println("expectedMediaDataLength = " + expectedMediaDataLength);
		// hmm, perhaps the SampleToChunkBox only has new entries when the description of the chunks changes.
		// the first two are different, then it's the same until chunk 36.
		// in that case, we're supposed to iterate over the chunks, taking as much as is described by the corresponding sampletochunk box

		// todo: we end up only reading about a third of the entry sizes. why is that?
		// it's because we're doing stsc.ec*chunkOffset.samplesPerChunk reads only.
		// clearly this is insufficient. what is left, then? How can we reach the rest?
		return chunks;
	}

	private int sum(List<Integer> entrySizes, int lastSample, int samplesPerChunk) {
		int sum = 0;
		for (int i = lastSample; i < lastSample + samplesPerChunk; i++) {
			sum += entrySizes.get(i);
			//System.out.println("Getting entry size: " + i);
		}
		return sum;
	}

	private static class Chunk {
		private final long chunkOffset;
		private final int chunkSize;

		private Chunk(long chunkOffset, int chunkSize) {
			this.chunkOffset = chunkOffset;
			this.chunkSize = chunkSize;
		}
	}
}
