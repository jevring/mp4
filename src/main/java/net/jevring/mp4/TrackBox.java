package net.jevring.mp4;

import net.jevring.mp4.boxes.*;

/**
 * @author markus@jevring.net
 */
public class TrackBox extends Box {
	public enum Type {
		Video("vide"),
		Audio("soun");
		
		private final String type;

		private Type(String type) {
			this.type = type;
		}
	}
	public TrackBox(int size, Box container) {
		super(size, "trak", container, true);
	}
	
	public int getTrackId() {
		Box b = this;
		b = b.getChildOfType("tkhd");
		TrackHeaderBox tkhd = (TrackHeaderBox) b;
		return tkhd.getTrackId();
	}

	public Type getTrackType() {
		// todo: perhaps we should have some simple way of addressing this, like /ftyp/moov/trak[i]/hdlr, etc...
		Box mdia = getChildOfType("mdia");
		if (mdia != null) {
			Box b = mdia.getChildOfType("hdlr");
			if (b != null) {
				HandlerBox hdlr = (HandlerBox)b;
				if (Type.Video.type.equals(hdlr.getHandlerType())) {
					return Type.Video;
				} else if (Type.Audio.type.equals(hdlr.getHandlerType())) {
					return Type.Audio;
				} else {
					throw new IllegalArgumentException("Unknown track type: " + hdlr.getHandlerType());
				}
			}
		}
		throw new IllegalArgumentException("Could not found 'hdlr' box");
	}

	public SampleToChunkBox getSampleToChunkBox() {
		Box b = this;
		b = b.getChildOfType("mdia");
		b = b.getChildOfType("minf");
		b = b.getChildOfType("stbl");
		b = b.getChildOfType("stsc");
		return (SampleToChunkBox)b;
	}

	public SampleSizeBox getSampleSizeBox() {
		Box b = this;
		b = b.getChildOfType("mdia");
		b = b.getChildOfType("minf");
		b = b.getChildOfType("stbl");
		b = b.getChildOfType("stsz");
		return (SampleSizeBox)b;
	}

	public ChunkOffsetBox getChunkOffsetBox() {
		Box b = this;
		b = b.getChildOfType("mdia");
		b = b.getChildOfType("minf");
		b = b.getChildOfType("stbl");
		b = b.getChildOfType("stco");
		return (ChunkOffsetBox)b;
	}

	public void setChunkOffsetBox(ChunkOffsetBox chunkOffsetBox) {
		Box b = this;
		b = b.getChildOfType("mdia");
		b = b.getChildOfType("minf");
		b = b.getChildOfType("stbl");
		b.setChildOfType("stco", chunkOffsetBox);

	}


}
