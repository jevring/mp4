package net.jevring.mp4;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * @author markus@jevring.net
 */
public class BoxWriter {
	private final DataOutputStream out;
	private long offset = 0;

	public BoxWriter(OutputStream out) {
		this.out = new DataOutputStream(out);
	}

	/**
	 * Writes an integer as a 4-byte array.
	 *
	 * @param i the integer to write
	 */
	public void writeSingleByteInteger(int i) throws IOException {
		out.write(i);
		out.flush();
	}

	/**
	 * Writes an integer as a 4-byte array.
	 *
	 * @param i the integer to write
	 */
	public void writeInteger(int i) throws IOException {
		out.writeInt(i);
		out.flush();
	}

	/**
	 * Writes an integer as a 2-byte array.
	 *
	 * @param s the short to write
	 */
	public void writeShort(short s) throws IOException {
		out.writeShort(s);
		out.flush();
	}

	/**
	 * Writes an long as a 8-byte array.
	 *
	 * @param l the long to write
	 */
	public void writeLong(long l) throws IOException {
		out.writeLong(l);
		out.flush();
	}

	/**
	 * Writes an string as a 4-byte array.
	 *
	 * @param s the string to write
	 */
	public void write4ByteString(String s) throws IOException {
		out.writeBytes(s);
		out.flush();
	}

	/**
	 * Writes an integer as a n-byte array where s.length() is n-1.
	 *
	 * @param s the string to write
	 */
	public void writeNullTerminatedString(String s) throws IOException {
		out.writeBytes(s);
		out.writeByte(0);
		out.flush();
	}

	public void writeBytes(byte[] bytes) throws IOException {
		out.write(bytes);
		out.flush();
	}

	public void writeIntegers(Integer... ints) throws IOException {
		for (Integer integer : ints) {
			out.writeInt(integer);
		}
		out.flush();
	}

	/**
	 * Writes a specified number of 0 bytes. Equivalent of reserved or skipped bytes when reading.
	 *
	 * @param i the number of bytes to write
	 */
	public void skip(int i) throws IOException {
		byte[] b = new byte[i];
		out.write(b);
		out.flush();
	}

	public long getOffset() {
		return out.size();
	}

	/**
	 * Writes a string with padding totalling i length, including a short at the front indicating how long the string is without padding
	 *
	 * @param string the string to write
	 * @param i      how long the total padded section should be
	 */
	public void writePaddedString(String string, int i) throws IOException {
		String s = string.trim();
		writeShort((short) s.length());
		out.writeBytes(s);
		byte[] padding = new byte[i - 2 - s.length()];
		out.write(padding);

	}
}
