package net.jevring.mp4;

import net.jevring.mp4.boxes.MP4File;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @author markus@jevring.net
 */
public class Main {
	public static void main(String[] args) {
		String input = args[0];

		String output = null;
		if (args.length > 1) {
			output = args[1];
		}
		System.out.println("Reading: " + input);
		System.out.println("Writing: " + output);
		MP4File file = new MP4File(input);
		Printer printer = new Printer();

		Reader reader = null;
		try (FileInputStream fis = new FileInputStream(file.getPath().toFile())) {
			reader = new Reader(fis, file);
			reader.read();
			printer.print(file);
		} catch (IOException e) {
			e.printStackTrace();
		}

		if (output != null) {
			try (FileOutputStream fos = new FileOutputStream(output)) {
				Demuxer demuxer = new Demuxer(file, file.getTrack(1));
				demuxer.write(new BoxWriter(fos));
				//Writer writer = new Writer(fos);
				//writer.write(file.getBoxes());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
